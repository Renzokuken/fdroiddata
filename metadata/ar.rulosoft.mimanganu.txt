Categories:Reading
License:MIT
Web Site:
Source Code:https://github.com/raulhaag/MiMangaNu
Issue Tracker:https://github.com/raulhaag/MiMangaNu/issues

Auto Name:Mi Manga Nu
Summary:Manga reader
Description:
Read and organize mangas.
.

Repo Type:git
Repo:https://github.com/raulhaag/MiMangaNu

Build:1.0,1
    disable=force push by upstream
    commit=e59f73b592cb24127c8dcb5e0780a9ec61425780
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.03,3
    commit=80e127a678f6d48b090d5918e03961466fe5d9bc
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.06,6
    commit=95d27bf735e9c0329711991f31942d5940d0fc99
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.07,7
    commit=d9284b1acffa7a005cd713f2973650b15aabae18
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.08,8
    commit=9f3135ddc34f5b1703266b5e03ac45ecb51835f0
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.09,9
    commit=52b202ff12e7502e75cba78da851b55b00069470
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.10,10
    commit=e5891dbe5d4c8024d87a3841266c2ef66bc8b730
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.11,11
    commit=c18f83c59c68c1fceb4e7f59a433375b059f05b2
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.12,12
    commit=912544c76ad85373e249e0c5d8043b9276e5d4a0
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.13,13
    commit=7acdcb78171073f274aa1e1c14c3f07af1c21e28
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.14,14
    commit=0f1cc5f1ad3ab418e4ccdd53b8c0dacb00165767
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.15,15
    commit=bdef42d1b3b21a14f47671673d00a7daffeb83fb
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.16,16
    commit=47bbc9bf86f7d04039fa723acec9053176ad6283
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.17,17
    commit=0825bbf43e28af1418859490fa6d9afebcbabd4f
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Build:1.18,18
    commit=51d2fb4057120b9afb5a9789b5df88875cc2116b
    subdir=MiMangaNu
    srclibs=1:appcompat@v7
    prebuild=sed -i -e '/ImageViewZoom/d' project.properties && \
        echo "android.library.reference.2=../ImageViewTouchLibrary" >> project.properties

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.18
Current Version Code:18

