Categories:Games,Internet
License:LGPLv2.1+,CC-BY-SA 3.0
Web Site:http://minetest.net
Source Code:https://github.com/minetest/minetest
#Issue Tracker:http://wiki.minetest.net/Reporting_bugs
Donate:http://minetest.net/donate

Name:Minetest
Summary:Infinite-world block sandbox game
Description:
Minetest is an infinite-world block sandbox game and a game engine, inspired by InfiniMiner, Minecraft and the like.

Features:
- Explore, dig and build in a voxel world, and craft stuff from raw materials to help you along the way.
- Play with your friends on public servers or self hosted servers
- Easy plugin based Modding API used to add blocks, tools and features to the game.
- Voxel based lighting with gameplay consequences (light caves and buildings with torches)
- Almost infinite world and several beautiful map generators.
- Runs natively on Windows, Linux, OS X, FreeBSD and Android
- Supports multiple languages, translated by the community.
- A constant development to add new functionalities for end-users
.
Repo Type:git
Repo:https://github.com/minetest/minetest.git

Build:0.4.12.12,12
    disable=doesn't build for me
    commit=315b00d15081d1f56f0e2de22a4ff1a393ab7f22
    prebuild=mkdir ../../games/minetest_game && cp -R $$MinetestGame$$/* ../../games/minetest_game
    output=bin/Minetest-release-unsigned.apk
    subdir=build/android
    srclibs=MinetestGame@03c00a831d5c2fd37096449bee49557879068af1
    buildjni=no
    build=printf "%s\n%s\n%s" \
            "ANDROID_NDK = $$NDK$$" \
            "NDK_MODULE_PATH = $$NDK$$/toolchains" \
            "SDKFOLDER = $$SDK$$" > path.cfg && \
        make release

Maintainer Notes:
* CRITICAL: Failed to get apk manifest information
* No UCM applies since it's not using AM.xml but a template (like Firefox etc.)
* Sometimes needs multiple build runs. Maybe re-add for-loop:
    for n in 1 2 3; do make release; break; done
.

Update Check Mode:None
Current Version:0.4.12
Current Version Code:12
