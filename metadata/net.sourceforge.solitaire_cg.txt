Categories:Games
License:Apache2
Web Site:http://solitairecg.sourceforge.net
Source Code:http://sourceforge.net/p/solitairecg/code
Issue Tracker:http://solitairecg.sourceforge.net/issues.php

Auto Name:SolitaireCG
Summary:Solitaire Card Games
Description:
SolitaireCG is an adaptation of Ken Magic's "[[com.kmagic.solitaire]]"
for devices with few hardware buttons.

Changes against the original include:

* Add Deal menu entry
* Avoid card loss if spider deal interrupted
* Add README and COPYING menu displays
* Replace application icon

The changes enable card dealing and playability in spider. The original card
graphics, which can scale to higher resolutions, have been retained.
.

Repo Type:git
Repo:git://git.code.sf.net/p/solitairecg/code

Build:1.14,452
    commit=SolitaireCG-1.14

Auto Update Mode:Version SolitaireCG-%v
Update Check Mode:Tags
Current Version:1.14
Current Version Code:452

