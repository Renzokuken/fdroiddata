Categories:Office
License:Apache2
Web Site:
Source Code:https://github.com/adrianchifor/Swiftnotes
Issue Tracker:https://github.com/adrianchifor/Swiftnotes/issues
Bitcoin:1NZGAU1rEs1zBpwdjmnBjiyRsJHfycWhSF
Donate:http://goo.gl/TQRlTa

Auto Name:Swiftnotes
Summary:Take notes
Description:
Note taking app focusing on simplicity and speed. It offers you a quick and
easy way to stay organized, capture your thoughts, reminders or anything
that's on your mind, any time, anywhere. No extra unnecessary features, just
notes.
.

Repo Type:git
Repo:https://github.com/adrianchifor/Swiftnotes

Build:2.0.1,3
    commit=12598e6f6e8531f54ea002cf15e125f515dc6704
    prebuild=mv java src
    target=android-21

Build:3.0.2,5
    commit=e4873ef1aec5367ad54a1c93262e050d2f5e8f29
    subdir=app
    gradle=yes

Build:3.0.3,6
    commit=3868ce2b0008e1ceb6601eb4e366efa136152d52
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:3.0.3
Current Version Code:6

