Categories:System
License:GPLv2+
Web Site:http://acdisplay.artemchep.com
Source Code:https://github.com/AChep/AcDisplay
Issue Tracker:https://github.com/AChep/AcDisplay/issues

Auto Name:AcDisplay
Summary:Handle new notifications with ease
Description:
AcDisplay is a new way of handling notifications in Android.
It will let you know about new notifications by showing a minimal, beautiful
screen, allowing you to open them directly from the lock screen. And if you
want to see what's going on, you can simply take your phone out of your pocket
to view all the latest notifications, in a similarly pleasing and minimalistic
manner.

Features:
* Great design and awesome performance.
* Active mode (uses the device's sensors to wake your device up when you need it.)
* Use AcDisplay as your lockscreen.
* Translated to most popular languages.
* Inactive hours (to save some battery.)
* Enable only while charging.
* Lots of other features such as: Blacklist, Dynamic background, Low-priority notifications and much more.

This app uses a precompiled library: XposedBridge.
.

Repo Type:git
Repo:https://github.com/AChep/AcDisplay.git

Build:2.3,21
    commit=65a96e9692604ff083f0839e8e62948144b4c70b
    subdir=project/AcDisplay
    gradle=localized

Build:2.4.5,29
    disable=our gradle pre-processing is causing issues
    commit=2.4.5
    subdir=project/AcDisplay
    gradle=localized

Build:3.0.16,48
    commit=3.0.16
    subdir=project/app
    gradle=releaseFlavor
    rm=project/app/src/main/libs/square-seismic*.jar
    prebuild=sed -i -e '/askForPasswords/,$d' -e '/String timeStamp/aString keySalt = UUID.randomUUID().toString();\nString keyEncrypted = "foo";\n/*' -e '/buildTypes/i*/' -e '/appcompat-v7/icompile "com.squareup:seismic:1.0.1"' build.gradle

Build:3.1,49
    commit=3.1
    subdir=project/app
    gradle=releaseFlavor
    rm=project/app/src/main/libs/square-seismic*.jar
    prebuild=sed -i -e '/askForPasswords/,$d' -e '/String timeStamp/aString keySalt = UUID.randomUUID().toString();\nString keyEncrypted = "foo";\n/*' -e '/buildTypes/i*/' -e '/appcompat-v7/icompile "com.squareup:seismic:1.0.1"' build.gradle

Build:3.1.1,50
    commit=3.1.1
    subdir=project/app
    gradle=releaseFlavor
    rm=project/app/src/main/libs/square-seismic*.jar
    prebuild=sed -i -e '/askForPasswords/,$d' -e '/String timeStamp/aString keySalt = UUID.randomUUID().toString();\nString keyEncrypted = "foo";\n/*' -e '/buildTypes/i*/' -e '/appcompat-v7/icompile "com.squareup:seismic:1.0.1"' build.gradle

Maintainer Notes:
* Our gradle pre-processing is causing harm to some conditional statements.
* Looks like "AUM: Version %v" can be set when gradle is fixed.
* Uses jar for XposedBridge. Enabling one build since we already shipped it once...
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:3.1.1
Current Version Code:50

