Categories:Office
License:GPLv3+
Web Site:https://github.com/kodejak/Hashr/blob/HEAD/README.md
Source Code:https://github.com/kodejak/Hashr
Issue Tracker:https://github.com/kodejak/Hashr/issues

Auto Name:Hashr
Summary:Calculate and compare checksum hashes
Description:
Generate checksums from texts or files, compare it to other checksums
and show if they match or not. Following checksums are supported:

* MD5
* SHA-1
* SHA-256

Currently a file manager must be present at the device to choose files
for generating checksums.
.

Repo Type:git
Repo:https://github.com/kodejak/Hashr

Build:1.1,11
    commit=80da887e622595d80f37342c3974e6c8cc1008e2
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.1
Current Version Code:11

