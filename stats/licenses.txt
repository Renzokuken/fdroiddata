GPL 26
PublicDomain 11
GNUFDL 1
GPLv3+ 146
Unknown 1
GPLv2 99
GPLv3 597
GPL/MPL 1
AGPLv3 11
NYSLv0.9982 1
EPL 1
ISC 6
NetBSD 1
GPL/Artistic 2
AGPL 7
MIT 137
CCBYSA 1
WTFPL 12
GNUFDL/CCBYSA 1
Missing 1
Expat 1
Fair License 1
FreeBSD 7
BSD 4
LGPLv2.1+,CC-BY-SA 3.0 1
GPLv2+ 58
Unlicense/Apache2 1
NetBSD/Apache2 1
Zlib 2
MPL2 10
GPLv3 or New BSD 1
EUPL 1
X11 1
Apache2 368
LGPLv2.1 2
LGPL 12
NewBSD 30
Apache2,CC-BY-SA 1
MPL 1
MPL1.1 1
Artistic2 3
CC0 1
Unlicense 2
AGPLv3+ 8
BSD/Apache 1
Beer License 2
LGPLv3 1
NCSA 1
